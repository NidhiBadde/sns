/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Nidhi
 */
public class FlightDirectory {
    
    private ArrayList<Flight> flightDirectory;

    public FlightDirectory() {
        
        this.flightDirectory  = new ArrayList<Flight>();
    }

    public ArrayList<Flight> getFlightDirectory() {
        return flightDirectory;
    }

    public void setFlightDirectory(ArrayList<Flight> flightDirectory) {
        this.flightDirectory = flightDirectory;
    }
    
    public Flight addFlights()
    {
        Flight flight = new Flight();
        flightDirectory.add(flight);
        return flight;
    }
    
    public void deleteFlight(Flight f)
    {
        flightDirectory.remove(f);
    }
    
    
    
    
    
            
    
}
