/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sid
 */
public class CustomerDirectory {
    
    private List<Customer> customerDirectory;

    public List<Customer> getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(List<Customer> customerDirectory) {
        this.customerDirectory = customerDirectory;
    }
    
    
  public CustomerDirectory(){
      this.customerDirectory = new ArrayList<Customer>();
  }  
  
  public Customer addCustomer(){
      Customer cust = new Customer();
      customerDirectory.add(cust);
      return cust;
  }
  
  public void removeCustomer(Customer cust){
      customerDirectory.remove(cust);
      
  }
    
}
