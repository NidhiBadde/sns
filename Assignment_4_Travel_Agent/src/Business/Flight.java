/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Nidhi
 */
public class Flight {
    
    private String AircraftType;
    private String AirlineName;
    private String FlightNumber;
    private String TravelSource;
    private String TravelDestination;
    private String Day;
    private String Time;
    private String AircraftID;
    private float EconomySeatPrice;
    private float FirstClassSeatPrice;
    private float BusinessClassSeatPrice;
    private int totalSeats;

    public int getTotalSeats() {
        return totalSeats;
    }

    public void setTotalSeats(int totalSeats) {
        this.totalSeats = totalSeats;
    }

    
    
    public String getAircraftID() {
        return AircraftID;
    }

    public void setAircraftID(String AircraftID) {
        this.AircraftID = AircraftID;
    }

    public float getEconomySeatPrice() {
        return EconomySeatPrice;
    }

    public void setEconomySeatPrice(float EconomySeatPrice) {
        this.EconomySeatPrice = EconomySeatPrice;
    }

    public float getFirstClassSeatPrice() {
        return FirstClassSeatPrice;
    }

    public void setFirstClassSeatPrice(float FirstClassSeatPrice) {
        this.FirstClassSeatPrice = FirstClassSeatPrice;
    }

    public float getBusinessClassSeatPrice() {
        return BusinessClassSeatPrice;
    }

    public void setBusinessClassSeatPrice(float BusinessClassSeatPrice) {
        this.BusinessClassSeatPrice = BusinessClassSeatPrice;
    }
    public String getAircraftType() {
        return AircraftType;
    }

    public void setAircraftType(String AircraftType) {
        this.AircraftType = AircraftType;
    }

    public String getAirlineName() {
        return AirlineName;
    }

    public void setAirlineName(String AirlineName) {
        this.AirlineName = AirlineName;
    }

    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setFlightNumber(String FlightNumber) {
        this.FlightNumber = FlightNumber;
    }

    public String getTravelSource() {
        return TravelSource;
    }

    public void setTravelSource(String TravelSource) {
        this.TravelSource = TravelSource;
    }

    public String getTravelDestination() {
        return TravelDestination;
    }

    public void setTravelDestination(String TravelDestination) {
        this.TravelDestination = TravelDestination;
    }

    public String getDay() {
        return Day;
    }

    public void setDay(String Day) {
        this.Day = Day;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String Time) {
        this.Time = Time;
    }
    
   @Override
   
   public  String toString()
    {
        return AircraftID;
    }
    
    
}
