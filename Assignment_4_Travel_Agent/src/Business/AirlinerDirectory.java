/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Nidhi
 */
public class AirlinerDirectory {
    
    private ArrayList<Airliner> airlinerDirectory;
    
    public ArrayList<Airliner> getAirlinerDirectory() {
        return airlinerDirectory;
    }

    public void setAirlinerDirectory(ArrayList<Airliner> airlinerDirectory) {
        this.airlinerDirectory = airlinerDirectory;
    }
    
    public AirlinerDirectory(){
        airlinerDirectory = new ArrayList<Airliner>();
    }
    
    public Airliner addAirliner(){
        Airliner airline = new Airliner();
       airlinerDirectory.add(airline);
        return airline;
    }
    
    public void removeAirliner(Airliner airline){
  airlinerDirectory.remove(airline);
    }
    
    

}
