/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Nidhi
 */
public class FlightConfiguration {
    
    private FlightDirectory flightDirectory;
    
    public FlightConfiguration(FlightDirectory flightDirectory)
    {
        this.flightDirectory = flightDirectory;
        
        addDefaultFlights();
    }

    private void addDefaultFlights()
    {
        Flight f1 = flightDirectory.addFlights();
        
        f1.setAircraftID("001");
        f1.setAircraftType("Boeing737");
        f1.setAirlineName("Qatar");
        f1.setFlightNumber("123");
        f1.setTravelSource("Boston");
        f1.setTravelDestination("Delhi");
        f1.setDay("Monday");
        f1.setTime("Morning");
        f1.setEconomySeatPrice(123);
        f1.setFirstClassSeatPrice(456);
        f1.setBusinessClassSeatPrice(789);
        f1.setTotalSeats(123);
        
        Flight f2 = flightDirectory.addFlights();
        
        f2.setAircraftID("002");
        f2.setAircraftType("Boeing731");
        f2.setAirlineName("Qatar");
        f2.setFlightNumber("123");
        f2.setTravelSource("Boston");
        f2.setTravelDestination("Delhi");
        f2.setDay("Monday");
        f2.setTime("Morning");
        f2.setEconomySeatPrice(123);
        f2.setFirstClassSeatPrice(456);
        f2.setBusinessClassSeatPrice(789);
        f2.setTotalSeats(123);
    }
}


