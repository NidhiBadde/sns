/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Sid
 */
public class CustomerConfigurator {
    
    private CustomerDirectory customerDirectory;
    
    public CustomerConfigurator(CustomerDirectory customerDirectory){
        this.customerDirectory = customerDirectory;
      
        addDefaultCustomer();
        
    }

    private void addDefaultCustomer() {
        
     Customer cust = customerDirectory.addCustomer();
     String number = "6173318474";
     long num = Long.parseLong(number);
     cust.setAge(27);
     cust.setContactNumber(num);
     cust.setCustomerName("Siddhant Rao");
     cust.setPassport("K6107559");
     cust.setEmailId("dattatray.siddhant@gmail.com");
     
     Customer cust1 = customerDirectory.addCustomer();
     String number1 = "3456789324";
     long num1 = Long.parseLong(number1);
     cust1.setAge(23);
     cust1.setContactNumber(num1);
     cust1.setCustomerName("Sanjana Anaokar");
     cust1.setPassport("P234567");
     cust1.setEmailId("sanjana.anaokar@gmail.com");     
    }
}
