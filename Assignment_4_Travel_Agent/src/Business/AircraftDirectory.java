/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Nidhi
 */
public class AircraftDirectory {

    private ArrayList<Aircraft> aircraftDirectory;
    
    public ArrayList<Aircraft> getAircraftDirectory() {
        return aircraftDirectory;
    }
    
    public void setAircraftDirectory(ArrayList<Aircraft> aircraftDirectory) {
        this.aircraftDirectory = aircraftDirectory;
    }
    
    public AircraftDirectory(){
        aircraftDirectory = new ArrayList<Aircraft>();
    }
    
   
    public Aircraft addAircraft(){
        Aircraft aircraft = new Aircraft();
       aircraftDirectory.add(aircraft);
        return aircraft;
    }
    
    public void removeAirliner(Aircraft aircraft){
    aircraftDirectory.remove(aircraft);
    }
    
}
