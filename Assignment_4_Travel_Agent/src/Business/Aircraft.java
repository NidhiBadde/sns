/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Abstract.Stakeholders;
/**
 *
 * @author Sid
 */
public class Aircraft {
    
     private String typeOfAircraft;
    private String aircraftID;
    private int seats;
    private String nameOfAirliner;

    public String getNameOfAircraft() {
        return nameOfAirliner;
    }

    public void setNameOfAircraft(String nameOfAirliner) {
        this.nameOfAirliner = nameOfAirliner;
    }

    public String getTypeOfAircraft() {
        return typeOfAircraft;
    }

    public void setTypeOfAircraft(String typeOfAircraft) {
        this.typeOfAircraft = typeOfAircraft;
    }

    public String getAircraftID() {
        return aircraftID;
    }

    public void setAircraftID(String aircraftID) {
        this.aircraftID = aircraftID;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
    
      @Override
     public String toString() {
         return this.aircraftID;
     }
    
    
}
